"""
docstring, ker pylint tak hoce...
"""

from datetime import datetime
from os import path

PATH = path.dirname(path.abspath(__file__))
FILENAME = "myOutput.txt"
PATH_FILENAME = path.join(PATH, FILENAME)

if __name__ == "__main__":
    print("Program zagnan\n")

    with open(PATH_FILENAME, "w", encoding="utf-8") as file:
        print("File", FILENAME, "ustvarjen")

        file.write("********************************************\n")
        file.write("Ime fila: ")
        file.write(FILENAME)
        file.write("\n")
        file.write("Lokacija: ")
        file.write(PATH)
        file.write("\n\n")

        CURR_DATETIME = datetime.now()
        file.write("Trenutni cas: ")
        file.write(CURR_DATETIME.time().strftime("%H:%M:%S"))
        file.write("\n")
        file.write("Trenutni datum: ")
        file.write(CURR_DATETIME.date().strftime("%d/%m/%Y"))
        file.write("\n********************************************")
        print("Vsebina zapisana\n")

    print("Program koncan\n")
    